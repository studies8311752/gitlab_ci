import os

def convert_to_int(a):
    if len(a) > 1:
        return 'Instance not char'
    try:
        return int(a)
    except:
        return 'Instance not convertible'

if __name__ == '__main__':
    a = os.getenv('input')
    print(convert_to_int(a))