CREATE TABLE IF NOT EXISTS students 
(
    student_id SERIAL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    date_of_birth DATE NOT NULL,
    record_book_id INT NOT NULL UNIQUE
);

INSERT INTO students (first_name, last_name, date_of_birth, record_book_id) VALUES
('Виктор', 'Петрушкин', '2005-05-12', 221),
('Петр', 'Иванов', '2005-03-03', 222),
('Роман', 'Волков', '2005-09-17', 223);

CREATE TABLE IF NOT EXISTS record_books
(
    record_id SERIAL PRIMARY KEY,
    book_id INT NOT NULL references students(record_book_id),
    subject VARCHAR(50) NOT NULL,
    exam_date DATE NOT NULL,
    grade INT NOT NULL,
    teacher VARCHAR(50) NOT NULL
);


INSERT INTO record_books (book_id, subject, exam_date, grade, teacher) VALUES
(221, 'Физика', '2023-12-25', 4, 'Иванов И.И.'),
(222, 'Физика', '2023-12-25', 5, 'Иванов И.И.'),
(223, 'Физика', '2023-12-25', 4, 'Иванов И.И.'),
(221, 'Информатика', '2023-12-27', 3, 'Семенов С.Р.'),
(222, 'Информатика', '2023-12-27', 4, 'Семенов С.Р.'),
(223, 'Информатика', '2023-12-27', 4, 'Семенов С.Р.'),
(221, 'Программирование', '2023-12-29', 4, 'Анечкин В.П.'),
(222, 'Программирование', '2023-12-29', 4, 'Анечкин В.П.'),
(223, 'Программирование', '2023-12-29', 5, 'Анечкин В.П.');
