import os
import psycopg2
def connection_to_db(postgres_url: str, query: str):
    try:
        connection = psycopg2.connect(postgres_url)
        cursor = connection.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        connection.commit()
        return result
    
    except Exception as e:
        print(e)
    
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()

if __name__ == '__main__':
    db_url = os.environ.get('postgres_url')
    query_ = '''SELECT DISTINCT subject, exam_date FROM record_books'''
    result = connection_to_db(postgres_url=db_url, query=query_)
    print(result)